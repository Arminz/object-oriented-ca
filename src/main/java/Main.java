import Infrastructure.RepositoryFactory;
import models.course_offering.CourseOffering;
import models.student.Student;
import models.student_semester.StudentSemester;

public class Main {

    public static void main(String[] args) {
        Student student = RepositoryFactory.getInstance().getStudentRepository().findById("810194388");
        StudentSemester semester = student.getLastSemester();

        CourseOffering courseOffering1 = RepositoryFactory.getInstance().getCourseOfferingRepository().findById("43214-2");
        CourseOffering courseOffering2 = RepositoryFactory.getInstance().getCourseOfferingRepository().findById("43215-1");
        CourseOffering courseOffering3 = RepositoryFactory.getInstance().getCourseOfferingRepository().findById("43216-3");

        semester.addEnrollment(courseOffering1);
        semester.addEnrollment(courseOffering2);
        semester.addEnrollment(courseOffering3);

        semester.dropEnrollment(courseOffering1);

        semester.commit();
    }

}
