package models.major;

public class SubMajor {

    private String name;
    private Major major;

    public SubMajor(String name, Major major) {
        this.name = name;
        this.major = major;
    }

    public boolean equals(SubMajor obj) {
        return name.equals(obj.name) && major.equals(obj.major);
    }
}
