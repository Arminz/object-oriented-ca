package models.major;

public class Major {

    private String name;

    public Major(String name) {
        this.name = name;
    }

    public boolean equals(Major obj) {
        return name.equals(obj.name);
    }
}
