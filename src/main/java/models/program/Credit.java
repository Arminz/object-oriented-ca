package models.program;

public class Credit {

    private Integer practical;
    private Integer theoretical;

    public Credit(Integer practical, Integer theoretical) {
        this.practical = practical;
        this.theoretical = theoretical;
    }

    public void add(Credit credit) {
        practical += credit.practical;
        theoretical += credit.theoretical;
    }

    private Integer sum() {
        return practical + theoretical;
    }

    public Boolean gt(Credit target) {
        return sum() > target.sum();
    }

    public void sub(Credit credit) {
        practical -= credit.practical;
        theoretical -= credit.theoretical;
    }

    public boolean gte(Credit target) {
        return sum() >= target.sum();
    }
}
