package models.program;

import Infrastructure.RepositoryFactory;
import models.course.Course;
import models.student_semester.CourseEnrollment;
import models.student_semester.CourseEnrollmentRepository;

import java.util.ArrayList;
import java.util.List;

public class ProgramItem extends ProgramBaseItem {

    private Course course;
    private ArrayList<Course> preConditions;
    private ArrayList<Course> inConditions;
    private Credit credit;
    private Credit minimumPassedCredit;
    private Credit maximumAcquiredCredit;

    public ProgramItem(Course course, Credit credit) {
        this.course = course;
        this.credit = credit;
        this.preConditions = new ArrayList<Course>();
        this.inConditions = new ArrayList<Course>();
    }

    public void setMinimumPassedCredit(Credit minimumPassedCredit) {
        this.minimumPassedCredit = minimumPassedCredit;
    }

    public void setMaximumAcquiredCredit(Credit maximumAcquiredCredit) {
        this.maximumAcquiredCredit = maximumAcquiredCredit;
    }

    public void addPreCondition(Course course) {
        preConditions.add(course);
    }

    public void addInCondition(Course course) {
        inConditions.add(course);
    }

    @Override
    public ProgramItem getItem(Course course) {
        if (this.course.equals(course))
            return this;
        return null;
    }

    @Override
    public Boolean passed(Course course) {
        if (this.course.equals(course)) {
            CourseEnrollmentRepository repo = RepositoryFactory.getInstance().getCourseEnrollmentRepository();
            CourseEnrollment courseEnrollment = repo.findByStudentAndCourse(course);
            if (courseEnrollment == null)
                return false;
            return courseEnrollment.getGrade().passed();
        }
        return false;
    }

    public Boolean hasPreconditions(List<Course> passedCourses) {
        for (Course course : preConditions) {
            if (!passedCourses.contains(course))
                return false;
        }
        return true;
    }

    public boolean hasInConditions(ArrayList<Course> allCourses) {
        for (Course course : inConditions) {
            if (!allCourses.contains(course))
                return false;
        }
        return true;
    }
}
