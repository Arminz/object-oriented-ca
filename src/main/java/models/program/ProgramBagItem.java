package models.program;

import models.course.Course;

import java.util.ArrayList;

public class ProgramBagItem extends ProgramBaseItem {

    private ArrayList<ProgramBaseItem> items;
    private Credit minimumCreditToBePassed;

    public ProgramBagItem() {
        items = new ArrayList<ProgramBaseItem>();
        minimumCreditToBePassed = new Credit(0, 0);
    }

    public void addItem(ProgramBaseItem item) {
        items.add(item);
    }

    public void setMinimumCreditToBePassed(Credit minimumCreditToBePassed) {
        this.minimumCreditToBePassed = minimumCreditToBePassed;
    }

    @Override
    public ProgramItem getItem(Course course) {
        for (ProgramBaseItem item : items) {
            ProgramItem chartItem = item.getItem(course);
            if (chartItem != null)
                return chartItem;
        }
        return null;
    }

    @Override
    public Boolean passed(Course course) {
        for (ProgramBaseItem item : items)
            if (item.passed(course))
                return true;
        return false;
    }
}
