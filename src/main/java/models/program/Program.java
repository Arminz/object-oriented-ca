package models.program;

import models.course.Course;
import models.date.AcademicYear;
import models.major.SubMajor;

import java.util.ArrayList;

public class Program {

    private String id;
    private SubMajor subMajor;
    private AcademicYear academicYear;
    private ArrayList<ProgramBaseItem> items;

    public Program(String id, SubMajor subMajor, AcademicYear academicYear) {
        this.id = id;
        this.subMajor = subMajor;
        this.academicYear = academicYear;
        this.items = new ArrayList<ProgramBaseItem>();
    }

    public void addItem(ProgramBaseItem item) {
        items.add(item);
    }

    public String getId() {
        return id;
    }

    public boolean has(SubMajor subMajor, AcademicYear academicYear) {
        return this.subMajor.equals(subMajor) && this.academicYear.equals(academicYear);
    }

    public ProgramItem getChartItem(Course course) {
        for (ProgramBaseItem chartBaseItem : items) {
            ProgramItem item = chartBaseItem.getItem(course);
            if (item != null)
                return item;
        }
        return null;
    }
}
