package models.program;

import Infrastructure.BaseRepository;
import models.date.AcademicYear;
import models.major.SubMajor;

public interface ProgramRepository extends BaseRepository<Program, String> {

    Program findBySubMajorAndAcademicYear(SubMajor subMajor, AcademicYear academicYear);

}
