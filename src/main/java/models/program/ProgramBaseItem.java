package models.program;

import models.course.Course;

public abstract class ProgramBaseItem {

    public abstract ProgramItem getItem(Course course);

    public abstract Boolean passed(Course course);

}
