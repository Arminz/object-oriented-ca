package models.person;

import Infrastructure.BaseRepository;

public interface PersonRepository extends BaseRepository<Person, Integer> {

}
