package models.person;

public class Person {

    private Integer id;
    private String firstName;
    private String lastName;
    private String nationalCode;
    private String phoneNumber;
    private String address;

    public Person(Integer id, String firstName, String lastName, String nationalCode, String phoneNumber, String address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationalCode = nationalCode;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }
}
