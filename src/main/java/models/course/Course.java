package models.course;

public class Course {

    private Integer code;
    private String name;

    public Course(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

}
