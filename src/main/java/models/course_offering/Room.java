package models.course_offering;

public class Room {

    private String name;
    private Integer capacity;

    public Room(String name, Integer capacity) {
        this.name = name;
        this.capacity = capacity;
    }
}
