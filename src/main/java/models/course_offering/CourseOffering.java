package models.course_offering;

import Infrastructure.RepositoryFactory;
import models.program.Credit;
import models.course.Course;
import models.date.TimeInterval;
import models.person.Teacher;
import models.semster.Semester;
import models.student_semester.CourseEnrollment;

import java.util.ArrayList;

public class CourseOffering {

    private String id;
    private Integer subCode;
    private Room room;
    private Integer capacity;
    private ArrayList<TimeInterval> classTimes;
    private TimeInterval examTime;
    private Teacher teacher;
    private Semester semester;
    private Course course;
    private ArrayList<CourseEnrollment> enrollments;

    public CourseOffering(
            String id,
            Integer subCode,
            Room room,
            Integer capacity,
            ArrayList<TimeInterval> classTimes,
            TimeInterval examTime,
            Teacher teacher,
            Semester semester,
            Course course
    ) {
        this.id = id;
        this.subCode = subCode;
        this.room = room;
        this.capacity = capacity;
        this.classTimes = classTimes;
        this.examTime = examTime;
        this.teacher = teacher;
        this.semester = semester;
        this.course = course;
        this.enrollments = new ArrayList<CourseEnrollment>();
    }

    public Credit getCredit() {
        return null;
    }

    public boolean satisfy(Credit enrolledCredit) {
        return false; //TODO
    }

    public boolean hasClassTimeConflict(CourseOffering courseOffering) {
        for (TimeInterval classTime : classTimes)
            if (classTime.overlaps(courseOffering.classTimes))
                return true;
        return false;
    }

    public boolean hasExamTimeConflict(CourseOffering courseOffering) {
        return examTime.overlaps(courseOffering.examTime);
    }

    public void loadEnrollments() {
        CourseOfferingRepository repository = RepositoryFactory.getInstance().getCourseOfferingRepository();
        enrollments = repository.findCourseEnrollmentsById(id);
    }

    public boolean hasCapacity() {
        if (enrollments.isEmpty())
            loadEnrollments();
        return capacity >  enrollments.size();
    }

    public String getId() {
        return id;
    }

    public Course getCourse() {
        return course;
    }

    public void addEnrollment(CourseEnrollment enrollment) {
        enrollments.add(enrollment);
    }

    public int remainedCapacity(){
        return capacity - enrollments.size();
    }

    public void removeEnrollment(CourseEnrollment courseEnrollment) {
        enrollments.remove(courseEnrollment);
    }
}
