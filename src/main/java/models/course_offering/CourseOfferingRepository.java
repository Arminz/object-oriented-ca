package models.course_offering;

import Infrastructure.BaseRepository;
import models.student_semester.CourseEnrollment;

import java.util.ArrayList;

public interface CourseOfferingRepository extends BaseRepository<CourseOffering, String> {

    ArrayList<CourseEnrollment> findCourseEnrollmentsById(String id);

}
