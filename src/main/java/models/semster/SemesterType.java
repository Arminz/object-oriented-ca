package models.semster;

public enum SemesterType {
    FALL,
    SPRING,
    SUMMER
}
