package models.semster;

import models.date.TimeInterval;

public class Semester {

    private Integer year;
    private SemesterType type;
    private TimeInterval registerTime;
    private TimeInterval addAndDropTime;
    private TimeInterval withdrawalTime;

    public Semester(
            Integer year,
            SemesterType type,
            TimeInterval registerTime,
            TimeInterval addAndDropTime,
            TimeInterval withdrawalTime
    ) {
        this.year = year;
        this.type = type;
        this.registerTime = registerTime;
        this.addAndDropTime = addAndDropTime;
        this.withdrawalTime = withdrawalTime;
    }
}
