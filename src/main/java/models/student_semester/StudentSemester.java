package models.student_semester;

import Infrastructure.EnrollmentService;
import models.course.Course;
import models.course_offering.CourseOffering;
import models.date.TimeInterval;
import models.program.Credit;
import models.student.Student;
import models.student_semester.enrollment_rule.*;

import java.util.ArrayList;

public class StudentSemester {

    private String id;
    private StudentSemesterStatus status;
    private Student student;
    private CreditRule creditRule;
    private TimeInterval registerTime;
    private TimeInterval addAndDropTime;
    private TimeInterval withdrawalTime;
    private ArrayList<Ticket> certificates;
    private ArrayList<CourseEnrollment> courseEnrollments;
    private ArrayList<BaseRule> enrollmentRules;

    public StudentSemester(
            String id,
            Student student,
            CreditRule creditRule,
            TimeInterval registerTime,
            TimeInterval addAndDropTime,
            TimeInterval withdrawalTime
    ) {
        this.id = id;
        this.creditRule = creditRule;
        this.status = StudentSemesterStatus.INIT;
        this.student = student;
        this.registerTime = registerTime;
        this.addAndDropTime = addAndDropTime;
        this.withdrawalTime = withdrawalTime;
        this.courseEnrollments = new ArrayList<>();

        this.enrollmentRules = new ArrayList<>();
        this.enrollmentRules.add(new CapacityRule(this));
        this.enrollmentRules.add(new ClassTimeRule(this));
        this.enrollmentRules.add(new CourseMaxAquiredCreditRule(this));
        this.enrollmentRules.add(new CourseStatusRule(this));
        this.enrollmentRules.add(new ExamTimeRule(this));
        this.enrollmentRules.add(new MaxCreditRule(this));
        this.enrollmentRules.add(new StudentConditionRule(this));
    }

    public Boolean addEnrollment(CourseOffering courseOffering) {
        for (BaseRule enrollmentRule : enrollmentRules)
            if (!enrollmentRule.apply(courseOffering))
                return false;

        CourseEnrollment courseEnrollment = EnrollmentService.getEnrollment(courseOffering);
        courseEnrollments.add(courseEnrollment);

        return true;
    }

    public boolean dropEnrollment(CourseOffering courseOffering) {
        CourseEnrollment enrollment = null;
        for (CourseEnrollment courseEnrollment : courseEnrollments) {
            if (courseEnrollment.getCourseOffering().equals(courseOffering))
                enrollment = courseEnrollment;
        }
        if (enrollment == null)
            return false;

        Credit courseCredit = courseOffering.getCredit();
        Credit enrolledCredit = calcEnrolledCredits();

        Credit newEnrolledCredit = new Credit(0, 0);
        newEnrolledCredit.add(enrolledCredit);
        newEnrolledCredit.sub(courseCredit);
        if (!creditRule.satisfyMinCredit(newEnrolledCredit)) {
            return false;
        }

        courseEnrollments.remove(enrollment);

        for (CourseEnrollment courseEnrollment : courseEnrollments) {
            if (!student.hasConditions(courseEnrollment.getCourseOffering())) {
                courseEnrollments.add(enrollment);
                return false;
            }
        }

        EnrollmentService.removeEnrollment(enrollment);
        return true;
    }

    public boolean commit() {
        Credit enrolledCredit = calcEnrolledCredits();
        if (!creditRule.satisfyMinCredit(enrolledCredit)) {
            for (CourseEnrollment enrollment : courseEnrollments) {
                courseEnrollments.remove(enrollment);
                EnrollmentService.removeEnrollment(enrollment);
            }
            return false;
        }
        status = StudentSemesterStatus.PENDING;
        return true;
    }

    public Credit calcEnrolledCredits() {
        Credit sum = new Credit(0, 0);
        for (CourseEnrollment courseEnrollment : courseEnrollments)
            sum.add(courseEnrollment.getCredit());
        return sum;
    }

    public Boolean hasClassTimeConflict(CourseOffering courseOffering) {
        for (CourseEnrollment courseEnrollment : courseEnrollments)
            if (courseEnrollment.hasClassTimeConflict(courseOffering))
                return true;
        return false;
    }

    public boolean hasExamTimeConflict(CourseOffering courseOffering) {
        for (CourseEnrollment courseEnrollment : courseEnrollments)
            if (courseEnrollment.hasExamTimeConflict(courseOffering))
                return true;
        return false;
    }


    public ArrayList<Course> getCourses() {
        ArrayList<Course> courses = new ArrayList<Course>();
        for (CourseEnrollment enrollment : courseEnrollments) {
            if (enrollment.isNotDropped())
                courses.add(enrollment.getCourse());
        }
        return courses;
    }

    public boolean isPending() {
        return status.equals(StudentSemesterStatus.INIT);
    }

    public CreditRule getCreditRule() {
        return creditRule;
    }

    public String getId() {
        return id;
    }

    public Student getStudent() {
        return student;
    }

    public StudentSemesterStatus getStatus() {
        return status;
    }
}
