package models.student_semester;

public enum EnrollmentStatus {
    PENDING, DROPPED, FINISHED
}
