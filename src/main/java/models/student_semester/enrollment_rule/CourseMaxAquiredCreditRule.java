package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.program.Credit;
import models.student_semester.StudentSemester;

public class CourseMaxAquiredCreditRule extends BaseRule {

    public CourseMaxAquiredCreditRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        Credit enrolledCredit = studentSemester.calcEnrolledCredits();
        return courseOffering.satisfy(enrolledCredit);
    }

}
