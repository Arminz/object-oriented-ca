package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.student.Student;
import models.student_semester.StudentSemester;

public class StudentConditionRule extends BaseRule {

    public StudentConditionRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        Student student = studentSemester.getStudent();
        return student.hasConditions(courseOffering);
    }
}
