package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.student_semester.StudentSemester;

public class ExamTimeRule extends BaseRule {

    public ExamTimeRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        return !studentSemester.hasExamTimeConflict(courseOffering);
    }

}
