package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.student_semester.StudentSemester;
import models.student_semester.StudentSemesterStatus;

public class CourseStatusRule extends BaseRule {

    public CourseStatusRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        StudentSemesterStatus status = studentSemester.getStatus();
        return status.isAllowedToEnroll();
    }

}
