package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.student_semester.StudentSemester;

abstract public class BaseRule {

    protected StudentSemester studentSemester;

    BaseRule(StudentSemester studentSemester) {
        this.studentSemester = studentSemester;
    }

    abstract public boolean apply(CourseOffering courseOffering);

}
