package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.student_semester.StudentSemester;

public class CapacityRule extends BaseRule {

    public CapacityRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        return courseOffering.hasCapacity();
    }

}
