package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.student_semester.StudentSemester;

public class ClassTimeRule extends BaseRule {

    public ClassTimeRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        return !studentSemester.hasClassTimeConflict(courseOffering);
    }

}
