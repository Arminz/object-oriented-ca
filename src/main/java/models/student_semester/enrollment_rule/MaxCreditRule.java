package models.student_semester.enrollment_rule;

import models.course_offering.CourseOffering;
import models.program.Credit;
import models.student_semester.CreditRule;
import models.student_semester.StudentSemester;

public class MaxCreditRule extends BaseRule {

    public MaxCreditRule(StudentSemester studentSemester) {
        super(studentSemester);
    }

    @Override
    public boolean apply(CourseOffering courseOffering) {
        Credit courseCredit = courseOffering.getCredit();
        Credit enrolledCredit = studentSemester.calcEnrolledCredits();

        Credit newEnrolledCredit = new Credit(0, 0);
        newEnrolledCredit.add(courseCredit);
        newEnrolledCredit.add(enrolledCredit);

        CreditRule creditRule = studentSemester.getCreditRule();
        return creditRule.satisfyMaxCredit(newEnrolledCredit);
    }

}
