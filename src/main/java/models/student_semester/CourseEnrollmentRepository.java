package models.student_semester;

import Infrastructure.BaseRepository;
import models.course.Course;
import models.student.Student;

public interface CourseEnrollmentRepository extends BaseRepository<CourseEnrollment, String> {

    CourseEnrollment findByStudentAndCourse(Course course);

}
