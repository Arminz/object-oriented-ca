package models.student_semester;

public enum StudentSemesterStatus {
    INIT, LEFT, PENDING, DROPPED, FINISHED;

    public boolean isAllowedToEnroll() {
        return this == INIT || this == PENDING;
    }
}
