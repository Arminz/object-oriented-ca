package models.student_semester;

import models.program.Credit;
import models.course.Course;
import models.course_offering.CourseOffering;

public class CourseEnrollment {

    private EnrollmentStatus status;
    private CourseOffering offering;
    private Grade grade;

    public CourseEnrollment(CourseOffering offering) {
        this.offering = offering;
        this.status = EnrollmentStatus.PENDING;
    }

    public Credit getCredit() {
        return null;
    }

    public Boolean hasClassTimeConflict(CourseOffering courseOffering) {
        return offering.hasClassTimeConflict(courseOffering);
    }

    public boolean hasExamTimeConflict(CourseOffering courseOffering) {
        return offering.hasExamTimeConflict(courseOffering);
    }

    public Course getCourse() {
        return offering.getCourse();
    }

    public boolean isNotDropped() {
        return !status.equals(EnrollmentStatus.DROPPED);
    }


    public CourseOffering getCourseOffering() {
        return offering;
    }

    public Grade getGrade() {
        return grade;
    }
}
