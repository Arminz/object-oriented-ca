package models.student_semester;

import Infrastructure.BaseRepository;

public interface StudentSemesterRepository extends BaseRepository<StudentSemester, String> {
}
