package models.student_semester;

public class Grade {

    private Double score;

    public Grade(Double score) {
        if (score < 0 || score > 20)
            throw new IllegalArgumentException("Grade should be in range [0, 20]");
        this.score = score;
    }

    public Boolean passed() {
        return score >= 10;
    }
}

