package models.student_semester;

import models.program.Credit;

public class CreditRule {

    private Credit minCredit;
    private Credit maxCredit;

    public CreditRule(Credit minCredit, Credit maxCredit) {
        this.minCredit = minCredit;
        this.maxCredit = maxCredit;
    }

    public Boolean satisfyMaxCredit(Credit target) {
        return !maxCredit.gt(target);
    }

    public boolean satisfyMinCredit(Credit target) {
        return target.gte(minCredit);
    }
}
