package models.date;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TimeInterval {

    private Date date;
    private long duration;
    private boolean isWeakly;

    public TimeInterval(Date date, long duration, boolean isWeakly) {
        this.date = date;
        this.duration = duration;
        this.isWeakly = isWeakly;
    }

    public boolean overlaps(TimeInterval timeInterval) {
        if (isWeakly) {
            Calendar c1 = Calendar.getInstance();
            c1.setTime(date);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(timeInterval.date);

            int dayOfWeek = c1.get(Calendar.DAY_OF_WEEK);
            int otherDayOfWeek = c2.get(Calendar.DAY_OF_WEEK);

            if (dayOfWeek != otherDayOfWeek)
                return true;

            int startHour = c1.get(Calendar.HOUR);
            int otherStartHour = c2.get(Calendar.HOUR);

            c1.setTime(getEnd());
            c2.setTime(timeInterval.getEnd());

            int endHour = c1.get(Calendar.HOUR);
            int otherEndHour = c2.get(Calendar.HOUR);

            return !(endHour < otherStartHour && startHour > otherEndHour);
        } else
            return !(timeInterval.getEnd().compareTo(date) < 0 || timeInterval.date.compareTo(getEnd()) > 0);
    }

    public boolean overlaps(ArrayList<TimeInterval> times) {
        for (TimeInterval time : times)
            if (overlaps(time))
                return true;
        return false;
    }

    private Date getEnd() {
        return new Date(date.getTime() + duration);
    }
}
