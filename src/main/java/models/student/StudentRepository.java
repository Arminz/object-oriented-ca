package models.student;

import Infrastructure.BaseRepository;

public interface StudentRepository extends BaseRepository<Student, String> {
}
