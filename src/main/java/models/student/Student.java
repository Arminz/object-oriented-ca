package models.student;

import Infrastructure.RepositoryFactory;
import models.program.Program;
import models.program.ProgramItem;
import models.course.Course;
import models.course_offering.CourseOffering;
import models.date.AcademicYear;
import models.major.SubMajor;
import models.person.Person;
import models.student_semester.StudentSemester;

import java.util.ArrayList;

public class Student {

    private Person person;
    private String studentNumber;
    private AcademicYear academicYear;
    private SubMajor subMajor;
    private ArrayList<StudentSemester> semesters;

    public Student(Person person, String studentNumber, AcademicYear academicYear) {
        this.person = person;
        this.studentNumber = studentNumber;
        this.academicYear = academicYear;
        this.semesters = new ArrayList<StudentSemester>();
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public Program getChart() {
        return RepositoryFactory.getInstance().getChartRepository()
                .findBySubMajorAndAcademicYear(subMajor, academicYear);
    }

    public boolean hasConditions(CourseOffering courseOffering) {
        Program chart = getChart();
        ProgramItem chartItem = chart.getChartItem(courseOffering.getCourse());
        if (chartItem == null)
            return false;
        if (!chartItem.hasPreconditions(getPassedCourses()))
            return false;
        if (!chartItem.hasInConditions(getAllCourses()))
            return false;
        return true;
    }

    private ArrayList<Course> getPassedCourses() {
        ArrayList<Course> courses = new ArrayList<Course>();
        for (StudentSemester semester : semesters) {
            if (!semester.isPending())
                courses.addAll(semester.getCourses());
        }
        return courses;

    }

    private ArrayList<Course> getAllCourses() {
        ArrayList<Course> courses = new ArrayList<Course>();
        for (StudentSemester semester : semesters) {
            courses.addAll(semester.getCourses());
        }
        return courses;
    }

    public StudentSemester getLastSemester() {
        return semesters.get(semesters.size() - 1);
    }

    public ArrayList<StudentSemester> getSemesters() {
        return semesters;
    }
}
