package Infrastructure;

import Infrastructure.persistence.*;
import models.program.ProgramRepository;
import models.course_offering.CourseOfferingRepository;
import models.person.PersonRepository;
import models.student.StudentRepository;
import models.student_semester.CourseEnrollmentRepository;
import models.student_semester.StudentSemesterRepository;

public class RepositoryFactory {

    private static RepositoryFactory instance;
    private PersonRepository personRepository;
    private ProgramRepository chartRepository;
    private StudentRepository studentRepository;
    private StudentSemesterRepository studentSemesterRepository;
    private CourseOfferingRepository courseOfferingRepository;
    private CourseEnrollmentRepository courseEnrollmentRepository;

    private RepositoryFactory() {
        personRepository = new PersonRepositoryImpl();
        chartRepository = new ProgramRepositoryImpl();
        studentRepository = new StudentRepositoryImpl();
        studentSemesterRepository = new StudentSemesterRepositoryImpl();
        courseOfferingRepository = new CourseOfferingRepositoryImpl();
        courseEnrollmentRepository = new CourseEnrollmentRepositoryImpl();
    }

    public static RepositoryFactory getInstance() {
        if (instance == null)
            instance = new RepositoryFactory();
        return instance;
    }

    public PersonRepository getPersonRepository() {
        return personRepository;
    }

    public ProgramRepository getChartRepository() {
        return chartRepository;
    }

    public StudentRepository getStudentRepository() {
        return studentRepository;
    }

    public StudentSemesterRepository getStudentSemesterRepository() {
        return studentSemesterRepository;
    }

    public CourseOfferingRepository getCourseOfferingRepository() {
        return courseOfferingRepository;
    }

    public CourseEnrollmentRepository getCourseEnrollmentRepository() {
        return courseEnrollmentRepository;
    }
}
