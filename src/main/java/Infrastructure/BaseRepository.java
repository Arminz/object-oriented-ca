package Infrastructure;

public interface BaseRepository<T, ID> {

    T findById(ID id);

    void save(T item);

}
