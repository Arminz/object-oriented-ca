package Infrastructure.persistence;


import models.course_offering.CourseOffering;
import models.course_offering.CourseOfferingRepository;
import models.student_semester.CourseEnrollment;

import java.util.ArrayList;
import java.util.HashMap;

public class CourseOfferingRepositoryImpl implements CourseOfferingRepository {

    private HashMap<String, CourseOffering> items = new HashMap<String, CourseOffering>();

    public CourseOffering findById(String id) {
        return items.get(id);
    }

    public void save(CourseOffering item) {
        items.put(item.getId(), item);
    }

    public ArrayList<CourseEnrollment> findCourseEnrollmentsById(String id) {
        return null;
    }

}
