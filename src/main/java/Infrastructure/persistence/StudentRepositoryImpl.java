package Infrastructure.persistence;

import models.student.Student;
import models.student.StudentRepository;

import java.util.HashMap;

public class StudentRepositoryImpl implements StudentRepository {

    private HashMap<String, Student> items = new HashMap<String, Student>();

    public Student findById(String id) {
        return items.get(id);
    }

    public void save(Student item) {
        items.put(item.getStudentNumber(), item);
    }
}
