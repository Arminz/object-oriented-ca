package Infrastructure.persistence;

import models.program.Program;
import models.program.ProgramRepository;
import models.date.AcademicYear;
import models.major.SubMajor;

import java.util.HashMap;
import java.util.Map;

public class ProgramRepositoryImpl implements ProgramRepository {

    private HashMap<String, Program> items = new HashMap<String, Program>();

    public Program findById(String id) {
        return items.get(id);
    }

    public void save(Program item) {
        items.put(item.getId(), item);
    }

    public Program findBySubMajorAndAcademicYear(SubMajor subMajor, AcademicYear academicYear) {
        for (Map.Entry<String, Program> item: items.entrySet()) {
            Program chart = item.getValue();
            if (chart.has(subMajor, academicYear))
                return chart;
        }
        return null;
    }
}
