package Infrastructure.persistence;

import models.person.Person;
import models.person.PersonRepository;

import java.util.HashMap;

public class PersonRepositoryImpl implements PersonRepository {

    private HashMap<Integer, Person> items = new HashMap<Integer, Person>();

    public Person findById(Integer id) {
        return items.get(id);
    }

    public void save(Person item) {
        items.put(item.getId(), item);
    }

}
