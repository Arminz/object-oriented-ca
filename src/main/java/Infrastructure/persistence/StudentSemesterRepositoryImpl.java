package Infrastructure.persistence;

import models.student_semester.StudentSemester;
import models.student_semester.StudentSemesterRepository;

import java.util.HashMap;

public class StudentSemesterRepositoryImpl implements StudentSemesterRepository {

    private HashMap<String, StudentSemester> items = new HashMap<String, StudentSemester>();

    public StudentSemester findById(String id) {
        return items.get(id);
    }

    public void save(StudentSemester item) {
        items.put(item.getId(), item);
    }

}
