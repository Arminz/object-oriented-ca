package Infrastructure.persistence;

import models.course.Course;
import models.student_semester.CourseEnrollment;
import models.student_semester.CourseEnrollmentRepository;

import java.util.HashMap;
import java.util.UUID;

public class CourseEnrollmentRepositoryImpl implements CourseEnrollmentRepository {

    private HashMap<String, CourseEnrollment> items = new HashMap<String, CourseEnrollment>();

    @Override
    public CourseEnrollment findByStudentAndCourse(Course course) {
        return null;
    }

    @Override
    public CourseEnrollment findById(String id) {
        return items.get(id);
    }

    @Override
    public void save(CourseEnrollment item) {
        items.put(UUID.randomUUID().toString(), item);
    }
}
