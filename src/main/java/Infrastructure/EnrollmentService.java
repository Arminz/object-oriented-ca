package Infrastructure;

import models.course_offering.CourseOffering;
import models.student_semester.CourseEnrollment;

public class EnrollmentService {
    public static CourseEnrollment getEnrollment(CourseOffering courseOffering){
        CourseEnrollment courseEnrollment = new CourseEnrollment(courseOffering);
        courseOffering.addEnrollment(courseEnrollment);
        return courseEnrollment;
    }
    public static boolean removeEnrollment(CourseEnrollment courseEnrollment){
        CourseOffering courseOffering = courseEnrollment.getCourseOffering();
        courseOffering.removeEnrollment(courseEnrollment);
        return true;
    }
}
